﻿using System;
using AddressBook;
using AddressBookContract;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace AddressBookTest
{    
    [TestClass]
    public class UserHandlerTests
    {
        private IAddressBookDB _mockDB;

        private ContactDTO _testContact1;
        private ContactDTO _testContact2;

        private UserDTO _testUser1;
        private UserDTO _testUser2;

        [TestInitialize]
        public void Init()
        {
            _mockDB = new MockAddressBookDB();
            _testContact1 = new ContactDTO()
            {
                Title = "Mrs",
                FirstName = "Lizzie",
                LastName = "Windsor",
                AddressLine1 = "Buckingham Palace",
                AddressLine2 = "Westminister",
                City = "London",
                PostalZipCode = "SW1A 1AA",
                Phone1AreaCode = "0191",
                Phone1CountryCode = "44",
                Phone1Number = "999999"
            };

            _testContact2 = new ContactDTO()
            {
                Title = "Mr",
                FirstName = "Phil",
                LastName = "Windsor",
                AddressLine1 = "Buckingham Palace",
                AddressLine2 = "Westminister",
                City = "London",
                PostalZipCode = "SW1A 1AA",
                Phone1AreaCode = "0191",
                Phone1CountryCode = "44",
                Phone1Number = "999999"
            };

            _testUser1 = new UserDTO
            {
                Email = "BorisJ@DowningStreet.com",
                Password = "TestPassword"
            };

            _testUser2 = new UserDTO
            {
                Email = "Theresa May",
                Password = "TestPassword"
            };
        }

        [TestMethod]
        public void TestAddingUser()
        {
            var userHandler = new UserHandler(_mockDB);

            UserDTO user = userHandler.RegisterUser(_testUser1);

            Assert.AreEqual(1, user.Id);
            Assert.AreEqual(1, _mockDB.Users.Count());
            Assert.AreEqual(_testUser1.Email, _mockDB.Users.FirstOrDefault().Email);
            Assert.IsNotNull(_mockDB.Users.FirstOrDefault().Hash);
            Assert.IsNotNull(_mockDB.Users.FirstOrDefault().Salt);
        }

        [TestMethod]
        [ExpectedException(typeof(AddressBookException), "An empty password is not valid.")]
        public void TestAddingUser_EmptyPassword()
        {
            var userHandler = new UserHandler(_mockDB);
            _testUser1.Password = "";
            UserDTO user = userHandler.RegisterUser(_testUser1);
        }

        [TestMethod]
        [ExpectedException(typeof(AddressBookException), "Invalid email address supplied not able to register user.")]
        public void TestAddingBadUsername()
        {
            var userHandler = new UserHandler(_mockDB);

            UserDTO user = userHandler.RegisterUser(_testUser2);

        }
    }
}
