﻿using AddressBook;
using System;
using System.Data.Entity;

namespace AddressBookTest
{
    public class MockContactSet : MockDbSet<Contact>
    {
    }

    public class MockUserSet : MockDbSet<User>
    {
        public override User Add(User item)
        {
            item.Id = base.Local.Count + 1;
            return base.Add(item);
        }
    }

    public class MockUserContactSet : MockDbSet<UserContact>
    {
        public override UserContact Add(UserContact item)
        {
            item.Id = base.Local.Count + 1;
            return base.Add(item);
        }
    }

    public class MockAddressBookDB : IAddressBookDB
    {
        public MockAddressBookDB()
        {
            Contacts = new MockContactSet();
            Users = new MockUserSet();
            UserContacts = new MockUserContactSet();
        }

        public IDbSet<UserContact> UserContacts { get; private set; }
        public IDbSet<Contact> Contacts { get; private set; }

        public IDbSet<User> Users { get; private set; }

        public int SaveChanges()
        {
            return 1; // 1 update to the database.
        }
    }
}
