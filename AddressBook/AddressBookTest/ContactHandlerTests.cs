﻿using System;
using AddressBook;
using AddressBookContract;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace AddressBookTest
{    
    [TestClass]
    public class ContactHandlerTests
    {
        private IAddressBookDB _mockDB;

        private ContactDTO _testContact1;
        private ContactDTO _testContact2;

        [TestInitialize]
        public void Init()
        {
            _mockDB = new MockAddressBookDB();
            _testContact1 = new ContactDTO()
            {
                Title = "Mrs",
                FirstName = "Lizzie",
                LastName = "Windsor",
                AddressLine1 = "Buckingham Palace",
                AddressLine2 = "Westminister",
                City = "London",
                Country = "UK",
                PostalZipCode = "SW1A 1AA",
                Phone1AreaCode = "0191",
                Phone1CountryCode = "44",
                Phone1Number = "999999"
            };

            _testContact2 = new ContactDTO()
            {
                Title = "Mr",
                FirstName = "Phil",
                LastName = "Windsor",
                AddressLine1 = "Buckingham Palace",
                AddressLine2 = "Westminister",
                City = "London",
                Country = "UK",
                PostalZipCode = "SW1A 1AA",
                Phone1AreaCode = "0191",
                Phone1CountryCode = "44",
                Phone1Number = "999999"
            };
        }

        [TestMethod]
        public void TestAddingSingleContact()
        {
            var addressBookHandler = new ContactHandler(_mockDB);

            Guid id = addressBookHandler.AddContact(_testContact1, 1);

            Assert.IsNotNull(id);
            Assert.AreEqual(1, _mockDB.Contacts.Count());
        }

        [TestMethod]
        public void TestLatLongLookup()
        {
            var addressBookHandler = new ContactHandler(_mockDB);

            Guid id = addressBookHandler.AddContact(_testContact1, 1);

            Assert.IsNotNull( id);
            Assert.AreEqual(51.501009, Math.Round(_mockDB.Contacts.FirstOrDefault().Latitude.Value, 6));
            Assert.AreEqual(-0.141588, Math.Round(_mockDB.Contacts.FirstOrDefault().Longitude.Value, 6));
        }

        [TestMethod]
        public void TestBadLatLongLookup()
        {
            var addressBookHandler = new ContactHandler(_mockDB);
            _testContact1.AddressLine1 = "Definitely Not An Address";
            _testContact1.Country = " ";
            _testContact1.AddressLine2 = " ";
            _testContact1.City = " ";
            _testContact1.PostalZipCode = "00000000000000";
            Guid id = addressBookHandler.AddContact(_testContact1, 1);

            Assert.IsNotNull(id);
            Assert.AreEqual(0, Math.Round(_mockDB.Contacts.FirstOrDefault().Latitude.Value, 6));
            Assert.AreEqual(0, Math.Round(_mockDB.Contacts.FirstOrDefault().Longitude.Value, 6));
        }

        [TestMethod]
        public void TestNoContactsForUser()
        {
            var userHandler = new UserHandler(_mockDB);
            var contactHandler = new ContactHandler(_mockDB);

            UserDTO user = userHandler.RegisterUser(new UserDTO
            {
                Email = "BorisJ@DowningStreet.com",
                Password = "TestPassword"
            });

            var contacts = contactHandler.GetContactsForUser(1);

            Assert.AreEqual(0, contacts.Count);
        }

        [TestMethod]
        public void TestGetContactsForUser()
        {
            var userHandler = new UserHandler(_mockDB);
            var contactHandler = new ContactHandler(_mockDB);
            
            UserDTO user = userHandler.RegisterUser(new UserDTO
            {
                Email = "BorisJ@DowningStreet.com",
                Password = "TestPassword"
            });

            Guid id1 = contactHandler.AddContact(_testContact1, 1);
            Guid id2 = contactHandler.AddContact(_testContact2, 1);

            var contacts = contactHandler.GetContactsForUser(1);

            Assert.AreEqual(2, contacts.Count);
            Assert.IsTrue(contacts.Where(c => c.FirstName == "Lizzie").Any());
            Assert.IsTrue(contacts.Where(c => c.FirstName == "Phil").Any());
        }
    }
}
