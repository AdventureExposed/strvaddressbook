﻿CREATE TABLE [dbo].[Contacts] (
    [Id]                UNIQUEIDENTIFIER NOT NULL,
    [Title]             VARCHAR (50)     NOT NULL,
    [FirstName]         VARCHAR (50)     NOT NULL,
    [LastName]          VARCHAR (50)     NOT NULL,
    [AddressLine1]      VARCHAR (200)    NOT NULL,
    [AddressLine2]      VARCHAR (200)    NULL,
    [City]              NVARCHAR (200)   NOT NULL,
    [Country]           NVARCHAR (200)   NOT NULL,
    [PostalZipCode]     VARCHAR (50)     NOT NULL,
    [Latitude]          FLOAT (53)       NULL,
    [Longitude]         FLOAT (53)       NULL,
    [Phone1CountryCode] VARCHAR (50)     NOT NULL,
    [Phone1AreaCode]    VARCHAR (50)     NOT NULL,
    [Phone1Number]      VARCHAR (50)     NOT NULL,
    [Phone2CountryCode] VARCHAR (50)     NULL,
    [Phone2AreaCode]    VARCHAR (50)     NULL,
    [Phone2Number]      VARCHAR (50)     NULL,
    CONSTRAINT [PK_dbo.Contacts] PRIMARY KEY CLUSTERED ([Id] ASC)
);




