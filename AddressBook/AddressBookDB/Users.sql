﻿CREATE TABLE [dbo].[Users] (
    [Id]    INT          IDENTITY (1, 1) NOT NULL,
    [Email] VARCHAR (50) NOT NULL,
    [Salt]  BINARY (16)  NOT NULL,
    [Hash]  BINARY (20)  NOT NULL,
    CONSTRAINT [PK_dbo.Users] PRIMARY KEY CLUSTERED ([Id] ASC)
);



GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Email]
    ON [dbo].[Users]([Email] ASC);

