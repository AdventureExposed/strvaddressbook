﻿CREATE TABLE [dbo].[UserContacts] (
    [Id]        INT              IDENTITY (1, 1) NOT NULL,
    [UserId]    INT              NULL,
    [ContactId] UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_dbo.UserContacts] PRIMARY KEY CLUSTERED ([Id] ASC)
);


