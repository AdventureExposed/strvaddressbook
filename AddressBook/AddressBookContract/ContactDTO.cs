﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;

namespace AddressBookContract
{
    /// <summary>
    /// A public contract to hold contact information.
    /// </summary>
    [DataContract]
    public class ContactDTO
    {
        /// <summary>
        /// The ID of the contact as a <see cref="Guid"/> only filled when object returned from service.
        /// </summary>
        [DataMember]
        public Guid Id { get; private set; }

        /// <summary>
        /// Title of the contact as a <see cref="string"/> i.e. Mr, Mrs, Ms, Dr etc.
        /// </summary>
        [DataMember]
        [Required]
        public string Title { get; set; }

        /// <summary>
        /// First name of the contact as a <see cref="string"/>.
        /// </summary>
        [DataMember]
        [Required]
        public string FirstName { get; set; }

        /// <summary>
        /// Last name of the contact as a <see cref="string"/>.
        /// </summary>
        [DataMember]
        [Required]
        public string LastName { get; set; }

        /// <summary>
        /// First line of the contact's address as a <see cref="string"/>.
        /// </summary>
        [DataMember]
        [Required]
        public string AddressLine1 { get; set; }

        /// <summary>
        /// Second line of the contact's address as a <see cref="string"/>.
        /// </summary>
        [DataMember]
        public string AddressLine2 { get; set; }
        
        /// <summary>
        /// The contact's city as a <see cref="string"/>.
        /// </summary>
        [DataMember]
        [Required]
        public string City { get; set; }

        /// <summary>
        /// The contact's country as a <see cref="string"/>.
        /// </summary>
        [DataMember]
        [Required]
        public string Country { get; set; }

        /// <summary>
        /// A contact's postal or zip code as a <see cref="string"/>.
        /// </summary>
        [DataMember]
        [Required]
        public string PostalZipCode { get; set; }

        /// <summary>
        /// A <see cref="double"/> representing the latitude of the contact's address.
        /// </summary>
        [DataMember]
        public double Latitude { get; set; }

        /// <summary>
        /// A <see cref="double"/> representing the longitude of the contact's address.
        /// </summary>
        [DataMember]
        public double Longitude { get; set; }

        /// <summary>
        /// Country code part of the 1st phone number stored as a string.
        /// </summary>
        [DataMember]
        [Required]
        public string Phone1CountryCode { get; set; }

        /// <summary>
        /// Area code part of the 1st phone number stored as a string.
        /// </summary>
        [DataMember]
        [Required]
        public string Phone1AreaCode { get; set; }

        /// <summary>
        /// Main part of the 1st phone number stored as a string.
        /// </summary>
        [DataMember]
        [Required]
        public string Phone1Number { get; set; }

        /// <summary>
        /// Country code part of the 2nd phone number stored as a string.
        /// </summary>
        [DataMember]
        public string Phone2CountryCode { get; set; }

        /// <summary>
        /// Area code part of the 2nd phone number stored as a string.
        /// </summary>
        [DataMember]
        public string Phone2AreaCode { get; set; }

        /// <summary>
        /// Main part of the 2nd phone number stored as a string.
        /// </summary>
        [DataMember]
        public string Phone2Number { get; set; }
    }
}
