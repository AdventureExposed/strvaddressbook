﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;

namespace AddressBookContract
{
    /// <summary>
    /// A public contract to hold user information.
    /// </summary>
    [DataContract]
    public class UserDTO
    {
        /// <summary>
        /// The username should always be an email address as a <see cref="string"/>.
        /// </summary>
        [DataMember]
        [Required]
        public string Email { get; set; }

        /// <summary>
        /// Supplied Password as a <see cref="string"/>.
        /// </summary>
        [DataMember]
        public string Password { get; set; }

        /// <summary>
        /// The Id of the user as created by the database.
        /// </summary>
        [DataMember]
        public int Id { get; private set; }
    }
}
