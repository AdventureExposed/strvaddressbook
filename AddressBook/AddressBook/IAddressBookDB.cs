﻿using System.Data.Entity;

namespace AddressBook
{
    public interface IAddressBookDB
    {
        IDbSet<UserContact> UserContacts { get; }
        IDbSet<Contact> Contacts { get; }
        IDbSet<User> Users { get; }
        int SaveChanges();
    }
}