﻿using AddressBookContract;
using System;
using System.Linq;
using System.Security.Cryptography;

namespace AddressBook
{
    /// <summary>
    /// This class contains methods related to managing users in the DB using the provided DB context.
    /// </summary>
    public class UserHandler : IDisposable
    {
        private const int HashSize = 20;
        private const int SaltSize = 16;
        private readonly CoreToDTOMapper<User, UserDTO> _coreToDTOMapper;

        private readonly IAddressBookDB _db;

        /// <summary>
        /// Constructor which instantiates a new <see cref="UserHandler"/> for
        /// a given address book DB connection.
        /// </summary>
        /// <param name="addressBookDB">An instance of an <see cref="IAddressBookDB"/>.</param>
        public UserHandler(IAddressBookDB addressBookDB)
        {
            _db = addressBookDB;
            _coreToDTOMapper = new CoreToDTOMapper<User, UserDTO>();
        }

        #region Public Methods

        /// <summary>
        /// A method for registering the given user in the DB.
        /// </summary>
        /// <param name="newUser">A <see cref="UserDTO"/> object containing the Username and Password to register.</param>
        /// <returns>The completed <see cref="UserDTO"/> object with password removed and ID added.</returns>
        public UserDTO RegisterUser(UserDTO newUser)
        {
            // Email must be valid and password not empty
            if (ValidEmail(newUser.Email) )
            {
                if(string.IsNullOrWhiteSpace(newUser.Password))
                    throw new AddressBookException("An empty password is not valid.");

                (byte[] salt, byte[] hash) = GetPasswordHash(newUser.Password);

                AddressBookException exception = null;
                var user = new User
                {
                    Email = newUser.Email,
                    Hash = hash,
                    Salt = salt
                };
                                
                try
                {
                    _db.Users.Add(user);
                    _db.SaveChanges();
                }
                catch (Exception ex)
                {
                    exception = new AddressBookException(ex.GetBaseException().Message);
                }                

                if (exception != null)
                {
                    if (exception.Message.Contains("duplicate"))
                    {
                        throw new AddressBookException("Duplicate username/emails are not allowed.");
                    }

                    throw exception;
                }

                return _coreToDTOMapper.Map(user); //it worked
            }
            else
            {
                throw new AddressBookException("Invalid email address supplied not able to register user.");
            }
        }

        /// <summary>
        /// Method to provide user authentication to the service code for user login.
        /// </summary>
        /// <param name="userIn">A <see cref="UserDTO"/> object passed to the service containing email and password.</param>
        /// <returns>
        ///     A <see cref="Tuple{T1, T2}"/> containing a <see cref="bool"/>, <see langword="true"/> if email and 
        /// password are correct and <see langword="false"/> otherwise; and the user ID as an <see cref="int"/>.
        /// </returns>
        public (bool, int) AuthenticateUser(UserDTO userIn)
        {
            // Get the user record from the database, email is unique so there
            // should only be one record.
            User user = _db.Users.Where(u => u.Email == userIn.Email).SingleOrDefault();

            if (user == null || user.Equals(default(User)))
                return (false, -1);

            // Get the hashed password bytes from DB record
            byte[] existingHash = user.Hash;
            int userId = user.Id;

            // Get the salt from the DB record
            byte[] salt = user.Salt;

            // Compute the hash on the password the user entered using the same salt
            (_, byte[] computedHash) = GetPasswordHash(userIn.Password, salt);

            // Compare the computed Hash with what's in the DB
            for (int i = 0; i < HashSize; i++)
                if (existingHash[i] != computedHash[i])
                    throw new UnauthorizedAccessException("Unauthorized login attempt.");

            return (true, userId);
        }

        /// <summary>
        /// Dispose of the <see cref="UserHandler"/> object making sure the DB connection is disposed.
        /// </summary>
        public void Dispose()
        {
            if (_db is IDisposable)
            {
                ((IDisposable)_db).Dispose();
            }
        }
#endregion //Public Methods


#region Private Methods
        // Method to get salt and hash for a given password and optionally pass the salt as well.
        private (byte[] salt, byte[] hash) GetPasswordHash(string password, byte[] salt = null)
        {
            if(salt == null)
            {
                using (var rng = new RNGCryptoServiceProvider())
                {
                    rng.GetBytes(salt = new byte[SaltSize]);
                }
            }

            byte[] hash = new byte[HashSize];
            using (var saltedHash = new Rfc2898DeriveBytes(password, salt, 100000))
            {
                hash = saltedHash.GetBytes(HashSize);
            }

            return (salt, hash);
        }

        // Validates email addresses using the System.Net.Mail library.
        private bool ValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }
    }
#endregion
}
