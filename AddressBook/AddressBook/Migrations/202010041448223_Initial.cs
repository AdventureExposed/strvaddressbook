﻿namespace AddressBook.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Contacts",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Title = c.String(nullable: false, maxLength: 50, unicode: false),
                        FirstName = c.String(nullable: false, maxLength: 50, unicode: false),
                        LastName = c.String(nullable: false, maxLength: 50, unicode: false),
                        AddressLine1 = c.String(nullable: false, maxLength: 200, unicode: false),
                        AddressLine2 = c.String(maxLength: 200, unicode: false),
                        City = c.String(nullable: false, maxLength: 200),
                        Country = c.String(nullable: false, maxLength: 200),
                        PostalZipCode = c.String(nullable: false, maxLength: 50, unicode: false),
                        Latitude = c.Double(),
                        Longitude = c.Double(),
                        Phone1CountryCode = c.String(nullable: false, maxLength: 50, unicode: false),
                        Phone1AreaCode = c.String(nullable: false, maxLength: 50, unicode: false),
                        Phone1Number = c.String(nullable: false, maxLength: 50, unicode: false),
                        Phone2CountryCode = c.String(maxLength: 50, unicode: false),
                        Phone2AreaCode = c.String(maxLength: 50, unicode: false),
                        Phone2Number = c.String(maxLength: 50, unicode: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UserContacts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Int(),
                        ContactId = c.Guid(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Email = c.String(nullable: false, maxLength: 50, unicode: false),
                        Salt = c.Binary(nullable: false, maxLength: 16, fixedLength: true),
                        Hash = c.Binary(nullable: false, maxLength: 20, fixedLength: true),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Email, unique: true);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Users", new[] { "Email" });
            DropTable("dbo.Users");
            DropTable("dbo.UserContacts");
            DropTable("dbo.Contacts");
        }
    }
}
