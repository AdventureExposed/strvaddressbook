﻿namespace AddressBook
{
    /// <summary>
    /// A static factory class with methods which construct new handlers for users and contacts.
    /// </summary>
    public static class AddressBookHandlerFactory
    {
        /// <summary>
        /// A method which provides a new <see cref="UserHandler"/> with a
        /// <see cref="AddressBookDB"/> connection.
        /// </summary>
        public static UserHandler CreateUserHandler()
        {
            return new UserHandler(new AddressBookDB());
        }

        /// <summary>
        /// A method which provides a new <see cref="ContactHandler"/> with a
        /// <see cref="AddressBookDB"/> connection.
        /// </summary>
        public static ContactHandler CreateContactHandler()
        {
            return new ContactHandler(new AddressBookDB());
        }
    }
}
