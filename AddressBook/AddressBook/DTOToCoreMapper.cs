﻿using AutoMapper;

namespace AddressBook
{
    internal class DTOToCoreMapper<Type, TypeDTO>
        where Type : class, new()
        where TypeDTO : class, new()
    {
        private readonly IMapper _mapper;

        public DTOToCoreMapper()
        {
            var config = new MapperConfiguration(cfg => cfg.CreateMap<TypeDTO, Type>());

            _mapper = config.CreateMapper();
        }

        public DTOToCoreMapper(MapperConfiguration cfg)
        {
            _mapper = cfg.CreateMapper();
        }

        public Type Map(TypeDTO dtoObject)
        {
            var target = new Type();
            _mapper.Map(dtoObject, target);

            return target;
        }
    }
}