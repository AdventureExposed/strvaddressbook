﻿using AutoMapper;

namespace AddressBook
{
    internal class CoreToDTOMapper<Type, TypeDTO>
        where Type : class, new()
        where TypeDTO : class, new()
    {
        private readonly IMapper _mapper;

        public CoreToDTOMapper()
        {
            var config = new MapperConfiguration(cfg => cfg.CreateMap<Type, TypeDTO>());

            _mapper = config.CreateMapper();
        }

        public TypeDTO Map(Type coreObject)
        {
            var targetDTO = new TypeDTO();
            _mapper.Map(coreObject, targetDTO);

            return targetDTO;
        }
    }
}