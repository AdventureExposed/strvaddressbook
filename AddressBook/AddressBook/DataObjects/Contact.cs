using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AddressBook
{
    public partial class Contact
    {
        public Contact()
        {
            Id = Guid.NewGuid();
        }

        [Key]
        public Guid Id { get; private set; }

        [Required]
        [StringLength(50)]
        public string Title { get; set; }

        [Required]
        [StringLength(50)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(50)]
        public string LastName { get; set; }

        [Required]
        [StringLength(200)]
        public string AddressLine1 { get; set; }

        [StringLength(200)]
        public string AddressLine2 { get; set; }

        [Required]
        [StringLength(200)]
        public string City { get; set; }

        [Required]
        [StringLength(200)]
        public string Country { get; set; }

        [Required]
        [StringLength(50)]
        public string PostalZipCode { get; set; }

        public double? Latitude { get; set; }

        public double? Longitude { get; set; }

        [Required]
        [StringLength(50)]
        public string Phone1CountryCode { get; set; }

        [Required]
        [StringLength(50)]
        public string Phone1AreaCode { get; set; }

        [Required]
        [StringLength(50)]
        public string Phone1Number { get; set; }

        [StringLength(50)]
        public string Phone2CountryCode { get; set; }

        [StringLength(50)]
        public string Phone2AreaCode { get; set; }

        [StringLength(50)]
        public string Phone2Number { get; set; }
    }
}
