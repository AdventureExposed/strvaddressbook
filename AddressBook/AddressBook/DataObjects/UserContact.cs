namespace AddressBook
{
    using System;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class UserContact
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public int? UserId { get; set; }

        public Guid? ContactId { get; set; }
    }
}
