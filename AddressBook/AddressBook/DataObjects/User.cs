namespace AddressBook
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class User
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        [Index(IsUnique =true)]
        public string Email { get; set; }

        [Required]
        [MaxLength(16)]
        public byte[] Salt { get; set; }

        [Required]
        [MaxLength(20)]
        public byte[] Hash { get; set; }
    }
}
