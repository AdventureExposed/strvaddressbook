namespace AddressBook
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class AddressBookDB : DbContext, IAddressBookDB
    {
        public AddressBookDB()
            : base("AddressBookDB")
        {
        }

        public virtual IDbSet<UserContact> UserContacts { get; set; }
        public virtual IDbSet<Contact> Contacts { get; set; }
        public virtual IDbSet<User> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Contact>()
                .Property(e => e.Title)
                .IsUnicode(false);

            modelBuilder.Entity<Contact>()
                .Property(e => e.FirstName)
                .IsUnicode(false);

            modelBuilder.Entity<Contact>()
                .Property(e => e.LastName)
                .IsUnicode(false);

            modelBuilder.Entity<Contact>()
                .Property(e => e.AddressLine1)
                .IsUnicode(false);

            modelBuilder.Entity<Contact>()
                .Property(e => e.AddressLine2)
                .IsUnicode(false);

            modelBuilder.Entity<Contact>()
                .Property(e => e.PostalZipCode)
                .IsUnicode(false);

            modelBuilder.Entity<Contact>()
                .Property(e => e.Phone1CountryCode)
                .IsUnicode(false);

            modelBuilder.Entity<Contact>()
                .Property(e => e.Phone1AreaCode)
                .IsUnicode(false);

            modelBuilder.Entity<Contact>()
                .Property(e => e.Phone1Number)
                .IsUnicode(false);

            modelBuilder.Entity<Contact>()
                .Property(e => e.Phone2CountryCode)
                .IsUnicode(false);

            modelBuilder.Entity<Contact>()
                .Property(e => e.Phone2AreaCode)
                .IsUnicode(false);

            modelBuilder.Entity<Contact>()
                .Property(e => e.Phone2Number)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.Salt)
                .IsFixedLength();

            modelBuilder.Entity<User>()
                .Property(e => e.Hash)
                .IsFixedLength();
        }
    }
}
