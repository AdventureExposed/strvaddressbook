﻿using AddressBookContract;
using AutoMapper;
using OpenCage.Geocode;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AddressBook
{
    /// <summary>
    /// A class containing methods related to managing contacts in DB using the provided DB context.
    /// </summary>
    public class ContactHandler: IDisposable
    {
        private const string OpenCageAPIKey = "23e24750873e46c89b82fc64010d34be";
        private readonly Geocoder _geocoder;
        private readonly DTOToCoreMapper<Contact, ContactDTO> _dtoToCoreMapper;
        private readonly CoreToDTOMapper<Contact, ContactDTO> _coreToDTOMapper;
        private readonly IAddressBookDB _db;

        /// <summary>
        /// Instantiates a new <see cref="ContactHandler"/> object for a given DB context
        /// and sets up a required <see cref="Geocoder"/> and AutoMapper classes
        /// <see cref="DTOToCoreMapper{Type, TypeDTO}"/> and <see cref="CoreToDTOMapper{Type, TypeDTO}"/>.
        /// </summary>
        /// <param name="addressBookDB">An instance of the <see cref="IAddressBookDB"/> context for the contact handler to use.</param>
        public ContactHandler(IAddressBookDB addressBookDB)
        {
            _db = addressBookDB;
            _geocoder = new Geocoder(OpenCageAPIKey);

            // Custom mapping to ignore the ID from ContactDTO -> Contact
            var config = new MapperConfiguration(
                cfg => cfg.CreateMap<ContactDTO, Contact>()
                    .ForMember(dest => dest.Id, opt => opt.Ignore())
                );

            _dtoToCoreMapper = new DTOToCoreMapper<Contact, ContactDTO>(config);
            _coreToDTOMapper = new CoreToDTOMapper<Contact, ContactDTO>();
        }

#region Public Methods

        /// <summary>
        /// This method adds new contacts to the address book for a given user.
        /// </summary>
        /// <param name="newContact">A <see cref="Contact"/> object holding the contact details.</param>
        /// <param name="userId">An <see cref="int"/> holding the ID of the user for whom the contact is being added.</param>
        /// <returns>A <see cref="Guid"/> containing the ID of the new contact added.</returns>
        public Guid AddContact(ContactDTO newContact, int userId)
        {
            Contact contact = _dtoToCoreMapper.Map(newContact);
            AddressBookException exception = null;

            FindLatLongForContact(contact);
                        
            try
            {
                _db.Contacts.Add(contact);
                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                exception = new AddressBookException(ex.Message);
            }            

            if (exception != null)
                throw exception;
            else
            {
                // It was successful so add the user contact entry
                AddUserContactEntry(contact.Id, userId);
                return contact.Id;
            }
        }

        /// <summary>
        /// A method which returns a list of all contacts for a given user's ID.
        /// </summary>
        /// <param name="userId">An <see cref="int"/> which is the key of the Users table.</param>
        /// <returns>A <see cref="List{T}"/> of <see cref="ContactDTO"/>.</returns>
        public List<ContactDTO> GetContactsForUser(int userId)
        {
            var contactList = from u in _db.Users
                       join uc in _db.UserContacts on u.Id equals uc.UserId 
                       join c in _db.Contacts on uc.ContactId equals c.Id
                       where u.Id == userId
                       select c;

            var outputList = new List<ContactDTO>();
            foreach(var contact in contactList)
            {
                outputList.Add(_coreToDTOMapper.Map(contact));
            }
            return outputList;
        }

        // Ensure the database object is disposed
        public void Dispose()
        {
            if (_db is IDisposable)
            {
                ((IDisposable)_db).Dispose();
            }
        }

#endregion //Public Methods

#region Private Methods

        /// <summary>
        /// Attempts to find the longitude and latitude of the given contact's address if the address
        /// is complete and valid.
        /// </summary>
        /// <param name="contact"></param>
        private void FindLatLongForContact(Contact contact)
        {
            var fullAddress = string.Concat(
                    contact.AddressLine1,
                    contact.AddressLine2,
                    contact.City,
                    contact.Country,
                    contact.PostalZipCode);
            if (string.IsNullOrWhiteSpace(fullAddress))
                return;

            try
            {
                var result = _geocoder.Geocode(fullAddress);

                if (result != null && result.Results.Length > 0)
                {
                    contact.Latitude = result.Results.FirstOrDefault().Geometry.Latitude;
                    contact.Longitude = result.Results.FirstOrDefault().Geometry.Longitude;
                }//else nothing happens
            }
            finally
            {
                //fail silently
            }
        }

        private void AddUserContactEntry(Guid contactId, int userId)
        {
            AddressBookException exception = null;
            try
            {
                _db.UserContacts.Add(new UserContact
                {
                    ContactId = contactId,
                    UserId = userId
                });
                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                exception = new AddressBookException($"Failed to add link between user ID {userId} and contact Id {contactId}: {ex.Message}");
            }

            if (exception != null)
                throw exception;
        }

        #endregion //Private Methods

    }
}
