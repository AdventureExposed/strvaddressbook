﻿using AddressBook;
using System.Linq;
using System.Security.Claims;
using System.Web.Http;

namespace AddressBookService.Controllers
{
    /// <summary>
    /// An abstract base controller which holds methods used by multiple other controllers.
    /// </summary>
    public abstract class BaseController : ApiController
    {
        /// <summary>
        /// A method which returns the User ID as an <see cref="int"/> of the current user.
        /// The current user is the one which the current valid JWT token was issued for.
        /// </summary>
        /// <returns>The ID of the current user as an <see cref="int"/>.<returns/>
        protected int GetUserId()
        {
            return int.Parse(((ClaimsIdentity)User.Identity).Claims.First(i => i.Type == "UserId").Value);
        }
    }
}
