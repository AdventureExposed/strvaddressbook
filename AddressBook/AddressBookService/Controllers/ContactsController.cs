﻿using AddressBook;
using AddressBookContract;
using AddressBookService.Jwt;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace AddressBookService.Controllers
{
    /// <summary>
    /// The Controller class for all API actions related to contacts.
    /// All contacts added or returned are for the user for which
    /// the current valid JWT token was issued.
    /// </summary>
    [JwtAuthentication]
    [RoutePrefix("api/contacts")]
    public class ContactsController : BaseController
    {
        private readonly ContactHandler _contactsHandler;

        /// <summary>
        /// Instantiates a new <see cref="ContactsController"/> and initialises 
        /// a new <see cref="ContactHandler"/> for CRUD operations related to Contacts.
        /// </summary>
        public ContactsController()
        {
            _contactsHandler = AddressBookHandlerFactory.CreateContactHandler();
        }

        /// <summary>
        /// A method for posting new contacts to the service for the current user.
        /// </summary>
        /// <param name="contact">The contact data to add stored in a <see cref="ContactDTO"/> object.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("")]
        public Guid PostContact(ContactDTO contact)
        {
            return _contactsHandler.AddContact(contact, base.GetUserId());
        }

        /// <summary>
        /// Gets all the contacts for the current user.
        /// </summary>
        /// <returns>A <see cref="List{T}"/> of <see cref="ContactDTO"/> objects.</returns>
        [HttpGet]
        [Route("{getall}")]
        public List<ContactDTO> GetAll()
        {
            return _contactsHandler.GetContactsForUser(base.GetUserId());
        }
    }
}
