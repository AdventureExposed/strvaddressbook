﻿using AddressBook;
using AddressBookContract;
using AddressBookService.Jwt;
using System;
using System.Net;
using System.Web.Http;

namespace AddressBookService.Controllers
{
    /// <summary>
    /// The Controller class for all API actions related to users.
    /// </summary>
    [RoutePrefix("api/users")]
    public class UsersController : BaseController
    {
        protected readonly UserHandler _usersHandler;

        /// <summary>
        /// Instantiates a new <see cref="UsersController"/> and constructs the 
        /// <see cref="UserHandler"/> for CRUD operations related to Users.
        /// </summary>
        public UsersController()
        {
            _usersHandler = AddressBookHandlerFactory.CreateUserHandler();
        }

        /// <summary>
        /// A method for registering new users. Required data is passed via the <see cref="UserDTO"/> object.
        /// This method allows anonymous calls.
        /// </summary>
        /// <param name="newUser">A <see cref="UserDTO"/> object containing the username and password of the new user.</param>
        /// <returns>A <see cref="UserDTO"/> with the ID of the new user added and the password set to <see langword="null"/>.</returns>
        [AllowAnonymous]
        [HttpPost]
        [Route("register")]
        public string RegisterUser(UserDTO newUser)
        {
            var user = _usersHandler.RegisterUser(newUser);
            if(user.Id > -1)
            {
                return GetToken(newUser);
            }
            throw new UnauthorizedAccessException("There was an error registering this user. Please contact support.");
        }

        /// <summary>
        /// A method for retrieving a JWT authentication token for a valid user.
        /// This method allows anonymous calls.
        /// </summary>
        /// <param name="user">A <see cref="UserDTO"/> of the user requesting the JWT token.</param>
        /// <returns>
        ///     A <see cref="string"/> containing the valid token if user is authenticated and 
        ///     and throws an <see cref="HttpStatusCode.Unauthorized"/> <see cref="HttpResponseException"/> otherwise.
        ///     </returns>
        [AllowAnonymous]
        [HttpGet]
        [Route("login")]
        public string GetToken(UserDTO user)
        {
            (var validUser, var userId) = CheckUser(user);
            if (validUser)
            {
                return JwtHandler.GenerateToken(user.Email, userId);
            }

            throw new UnauthorizedAccessException("Username/Email was not valid.");
        }

        /// <summary>
        /// An abstraction method for authenticating the provided email and password of a user.
        /// </summary>
        /// <param name="user">A <see cref="UserDTO"/> object containing the data of the user requiring authentication.</param>
        /// <returns>
        ///  A <see cref="Tuple{T1, T2}"/> containing a <see cref="bool"/>, <see langword="true"/> if email and 
        /// password are correct and <see langword="false"/> otherwise; and the user ID as an <see cref="int"/>.
        /// </returns>
        private (bool, int) CheckUser(UserDTO user)
        {
            return _usersHandler.AuthenticateUser(user);
        }
    }
}