﻿using AddressBook;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http.Filters;

/// <summary>
/// This is an implementation of a <see cref="ExceptionFilterAttribute"/> class which
/// is used to filter Exceptions thrown by the AddressBook core library. Most exceptions
/// will be exposed via the API as a <see cref="HttpStatusCode.BadRequest"/> or <see cref="HttpStatusCode.Unauthorized"/> request. 
/// Anything unknown will be exposed as an <see cref="HttpStatusCode.InternalServerError"/> and reported to the user with a generic
/// message.
/// 
/// </summary>
/// <remarks>No StackTrace will be exposed via the API.</remarks>
public sealed class HandleExeptionsAttribute : ExceptionFilterAttribute
{
    public override void OnException(HttpActionExecutedContext actionExecutedContext)
    {
        if (actionExecutedContext.Response != null)
        {
            return;
        }

        if (actionExecutedContext.Exception == null)
        {
            return;
        }

        if (actionExecutedContext.Exception is AddressBookException)
        {
            //log exception here

            actionExecutedContext.Response = 
                actionExecutedContext.Request.CreateErrorResponse(
                    HttpStatusCode.BadRequest, 
                    actionExecutedContext.Exception.Message);
        }
        else if(actionExecutedContext.Exception is UnauthorizedAccessException)
        {
            // log exception here

            actionExecutedContext.Response =
                actionExecutedContext.Request.CreateErrorResponse(
                    HttpStatusCode.Unauthorized, actionExecutedContext.Exception.Message);
        }
        else if (actionExecutedContext.Exception is TaskCanceledException == false && actionExecutedContext.Exception is OperationCanceledException == false)
        {
            // log exception here

            actionExecutedContext.Response = 
                actionExecutedContext.Request.CreateErrorResponse(
                    HttpStatusCode.InternalServerError, 
                "An unexpected error has occurred. Please contact the webmaster.");
        }
    }
}