﻿using System;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

/// <summary>
/// This is an implementation of the <see cref="AuthorizationFilterAttribute"/> class 
/// which is used to restrict calls to the api to SSL only.
/// </summary>
public class RequireHttpsAttribute : AuthorizationFilterAttribute
{
    public override void OnAuthorization(HttpActionContext actionContext)
    {
        if (actionContext.Request.RequestUri.Scheme != Uri.UriSchemeHttps)
        {
            actionContext.Response = new HttpResponseMessage(System.Net.HttpStatusCode.Forbidden)
            {
                ReasonPhrase = "HTTPS Required in order to use this service"
            };
        }
        else
        {
            base.OnAuthorization(actionContext);
        }
    }
}